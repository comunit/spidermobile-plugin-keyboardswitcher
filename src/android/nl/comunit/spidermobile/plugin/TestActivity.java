package android.nl.comunit.spidermobile.plugin;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import nl.comunit.spidermobile.plugin.R;

public class TestActivity extends Activity {

    private InputMethodManager inputMethodManager;

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public void toggleKeyboard (View view) {
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    public void isKeyboard (View view) {
        if (inputMethodManager != null) {
            String message;
            if (inputMethodManager.isActive()) {
                message = "keyboard visible";
            } else {
                message = "keyboard hidden";
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }
}
