package nl.comunit.spidermobile.plugin.keyboard;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author Mihai Pistol
 */
public class KeyboardSwitcher extends CordovaPlugin {

    private InputMethodManager inputMethodManager;

    @Override
    public void initialize (CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Logger.getLogger("KeyboardSwitcher").log(Level.INFO, "initialize()");
        inputMethodManager = (InputMethodManager) cordova.getActivity().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    public boolean execute (String action, JSONArray data, CallbackContext callback) throws JSONException {
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
            callback.success();
            return true;
        }
        callback.error("");
        return false;
    }
}
