/*jslint browser: true, continue: true, newcap: true, plusplus: true, unparam: true, todo: true, white: true */

/**
 * @author Mihai Pistol
 */
(function () {
    "use strict";

    function KeyboardSwitcher () {
    }

    /**
     * @function
     * @public
     * @memberOf {IMEIRetriver}
     * @param {Function} successHandler
     * @param {Function} failureHandler
     * @returns {undefined}
     */
    KeyboardSwitcher.prototype.switch = function (successHandler, failureHandler) {
        window.cordova.exec(successHandler, failureHandler, "KeyboardSwitcher", "", []);
    };

    KeyboardSwitcher.install = function () {
        if (!window.plugins) {
            window.plugins = {};
        }
        window.plugins.KeyboardSwitcher = new KeyboardSwitcher();
        return window.plugins.KeyboardSwitcher;
    };

    window.cordova.addConstructor(KeyboardSwitcher.install);
}());
